def is_triangle(a, b, c):
    try:
        a = float(a)
        b = float(b)
        c = float(c)
    except:
        return False
    
    if (a <= 0) or (b <= 0) or (c <= 0) or (b >= (a + c)) or (a >= (b + c)) or (c >= (a + b)):
        return False
    
    return True
